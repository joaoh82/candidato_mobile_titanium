function AppTabGroup() {
	//declare module dependencies
	var AppWindow = require('ui/AppWindow');
	var NewsView = require('windows/NewsView');
	var VideosView = require('windows/VideosView');
	var CalendarView = require('windows/CalendarView');
	var TwitterView = require('windows/TwitterView');
	var ProfileView = require('windows/CandidateProfileView');
	
	//create module instance
	var self = Ti.UI.createTabGroup();
	
	//create app tabs
	var win0 = new ProfileView(L('profile')),
		win1 = new NewsView(L('news'));
		win2 = new CalendarView(L('calendar'));
		win3 = new VideosView(L('video'));
		win4 = new TwitterView(L('twitter'));
	
	var tab0 = Ti.UI.createTab({
		title: L('profile'),
		icon: '/images/tab_profile.png',
		window: win0
	});
	win0.containingTab = tab0;
	
	var tab1 = Ti.UI.createTab({
		title: L('news'),
		icon: '/images/tab_news.png',
		window: win1
	});
	win1.containingTab = tab1;
	
	var tab2 = Ti.UI.createTab({
		title: L('calendar'),
		icon: '/images/tab_calendar.png',
		window: win2
	});
	win2.containingTab = tab2;
	
	var tab3 = Ti.UI.createTab({
		title: L('video'),
		icon: '/images/tab_video.png',
		window: win3
	});
	win3.containingTab = tab3;
	
	var tab4 = Ti.UI.createTab({
		title: L('twitter'),
		icon: '/images/tab_twitter.png',
		window: win4
	});
	win4.containingTab = tab4;
	
	self.addTab(tab0);
	self.addTab(tab1);
	self.addTab(tab2);
	self.addTab(tab3);
	self.addTab(tab4);
	
	return self;
};

module.exports = AppTabGroup;
