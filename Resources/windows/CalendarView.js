/**
 * @author Joao Henrique Machado Silva
 */

var objView = require('windows/SingleEventView');

var tempData = [{title:'Carregando...'}];
var table = Ti.UI.createTableView({data:tempData, backgroundColor:'transparent'});
var tableDataRaw = [];

function CalendarView(title) {
	var self = Ti.UI.createWindow({
		title:title,
		backgroundImage: 'images/bg.jpg',
		barColor:L('bar_color'),
		barImage: 'images/barraNavegacao.png'
	});
	
	table.addEventListener('click', function(e) {
    	Ti.API.log( tableDataRaw[e.index].Titulo );
    	Ti.API.log( tableDataRaw[e.index].ID );
		
		var obj = tableDataRaw[e.index];
		
		var win = new objView('Noticias', obj);
		self.containingTab.open(win);
	});
	
	self.add(table);
	
	getData();
	
	return self;
};

function getData() {
	var url = L('base_url') + L('get_eventos');
	var dataReturn = "";
	 var client = Ti.Network.createHTTPClient({
	     // function called when the response data is available
	     onload : function(e) {
	        // Ti.API.info("Received text: " + this.responseText);
	         //alert('success');
	         
	         parseJson(this.responseText);
	     },
	     // function called when an error occurs, including a timeout
	     onerror : function(e) {
	         Ti.API.debug(e.error);
	         //alert('error');
	         dataReturn = "error - check the logs";
	     },
	     timeout : 5000  // in milliseconds
	 });
	 
	 //creates parameters to send to the request
	 var params = {  
    'IDApp':L('id_app'),  
    'Token':L('token')
	}; 
	 // Prepare the connection.
	 client.open("POST", url);
	 // Send the request.
	 client.send(params);
	 
	 return dataReturn;
  
}

function parseJson(value){
	Ti.API.info('JSON CALENDER: ' + value);
	var jsonObject = JSON.parse(value);
	tableDataRaw = jsonObject.Aplicativo;
	var CustomData = [];
	
	for (var i=0; i < jsonObject.Aplicativo.length; i++) {
		CustomData.push({local:jsonObject.Aplicativo[i].Local, titulo:jsonObject.Aplicativo[i].Titulo, data:jsonObject.Aplicativo[i].Data ,hasChild:true});
		
		  Ti.API.info('ID: ' + jsonObject.Aplicativo[i].ID);
		  Ti.API.info('Titulo: ' + jsonObject.Aplicativo[i].Titulo);
		  Ti.API.info('Descricao: ' + jsonObject.Aplicativo[i].Descricao);
		  Ti.API.info('Local: ' + jsonObject.Aplicativo[i].Local);
		  Ti.API.info('Data: ' + jsonObject.Aplicativo[i].Data);
		  Ti.API.info('---------------------------------------');
	};
	Ti.API.info('CustomData Count: ' + CustomData.length);
	
	var tableData=[];
	for (var i=0; i < CustomData.length; i++) {
		
		var row = Titanium.UI.createTableViewRow({backgroundColor:'transparent'});
		 
		var thumb =  Titanium.UI.createImageView({
		url:'/images/schedule.png',
		width:48,
		height:48,
		left:4,
		top:4,
		bottom:4
		});
		 
		var titulo = Titanium.UI.createLabel({
		text:CustomData[i].titulo,
		font:{fontSize:16,fontWeight:'bold'},
		width:'auto',
		textAlign:'left',
		top:10,
		color : L('main_text_color'),
		left:60,
		height:'auto'
		});
		 
		var data =  Titanium.UI.createLabel({
		text:"Data: " + CustomData[i].data,
		font:{fontSize:12,fontWeight:'bold'},
		width:'auto',
		textAlign:'left',
		color : L('main_text_color'),
		bottom:5,
		left:200,
		height:'auto'
		});
		 
		row.add(thumb);
		row.add(titulo);
		row.add(data);
		row.hasChild=CustomData[i].hasChild;
		row.className = 'data_row';
		 
		tableData.push(row);
		Ti.API.info('tableData count: ' + tableData.length);
	};
		
	table.setData(tableData);

}

module.exports = CalendarView;