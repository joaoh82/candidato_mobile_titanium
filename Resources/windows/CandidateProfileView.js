/**
 * @author João Henrique Machado Silva
 */

var webview = Titanium.UI.createWebView({
	 	top:25,
	 	backgroundColor:'transparent'
	 	});

function CandidateProfileView(title) {
	
	var facebookButton = Titanium.UI.createButtonBar({
    labels:['Compartilhar com Facebook'],
    backgroundColor:'#336699',
    bottom:0,
    style:Titanium.UI.iPhone.SystemButtonStyle.BAR,
    height:25,
    width:200,
    left:(Titanium.Platform.displayCaps.platformWidth/2)-100
	});
	facebookButton.addEventListener('click', function(e) {
		shareWithFacebook();
	});
	
	var numero = Ti.UI.createLabel({
      font: { fontSize:L('partido_numero_size'), fontWeight: 'bold' },
      color: L('partido_numero_color'),
      text: L('numero'),
      textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
      top: 0,
      left: 10,
      width: 'auto', 
      height: 'auto'
    });
	
	var self = Ti.UI.createWindow({
		title:L('nome_candidato'),
		backgroundImage:'images/bg.jpg',
		barColor:L('bar_color'),
		barImage: 'images/barraNavegacao.png'
		//rightNavButton:facebookButton
	});
	
	var partido = Ti.UI.createLabel({
      font: { fontSize:L('partido_numero_size'), fontWeight: 'bold' },
      color: L('partido_numero_color'),
      text: L('partido'),
      textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
      top: 0,
      right: 10,
      width: 'auto', 
      height: 'auto'
    });
    
	 
	 webview.setHtml('<b>Carregando...</b>');
	 
	 self.add(webview);
	 self.add(partido);
	 self.add(numero);
	 self.add(facebookButton);
	 getData();
	
	
	
	return self;
};

function shareWithFacebook(){
	//AUTORIZANDO O FACEBOOK 
	Ti.Facebook.appid = L('facebook_id_app');
    Ti.Facebook.permissions = ['publish_stream']; // Permissions your app needs
    Ti.Facebook.addEventListener('login', function(e) {
        if (e.success) {

        } else if (e.error) {
            alert("Occoreu um erro no compartilhamento: "+e.error);
        } else if (e.cancelled) {
            
        }
    });
    Ti.Facebook.authorize();
    
    var data = {
        link : L('url_compartilhamento_facebook'),
        name : L('nome_candidato'),
        message : "Me mantendo informado com o aplicativo do candidato " + L('nome_candidato') + " já disponivel para iPhone e Android.",
        caption : "Maroca Prefeito - 45",
        picture : L('foto_compartilhamento_facebook'),
        description : "Me mantendo informado com o aplicativo do candidato " + L('nome_candidato') + " já disponivel para iPhone e Android."
    };
    Titanium.Facebook.dialog("feed", data, function(e) {
        if(e.success && e.result) {
            //alert("Success! New Post ID: " + e.result);
        } else {
            if(e.error) {
                alert("Occoreu um erro no compartilhamento: "+e.error);
            } else {
                //alert("User canceled dialog.");
            }
        }
    });
//     
    // Titanium.Facebook.requestWithGraphPath('me/feed', {message: "Me mantendo informado com o aplicativo do candidato " + L('nome_candidato') + " já disponivel para iPhone e Android."}, "POST", function(e) {
			    // if (e.success) {
			        // alert("Compartilhamento feito com sucesso!");
			    // } else {
			        // if (e.error) {
			            // alert("Occoreu um erro no compartilhamento: "+e.error);
			        // } else {
			            // alert("Occoreu um erro desconhecido no compartilhamento");
			        // }
			    // }
			// });
}

function getData() {
	//var url = "http://638ea652dec549f29d80802aae4a0894.cloudapp.net/WebServices/GetNoticias";
	var url = L('base_url') + L('get_perfil_candidato');
	var dataReturn = "";
	 var client = Ti.Network.createHTTPClient({
	     // function called when the response data is available
	     onload : function(e) {
	        // Ti.API.info("Received text: " + this.responseText);
	         //alert('success');
	         
	         parseJson(this.responseText);
	     },
	     // function called when an error occurs, including a timeout
	     onerror : function(e) {
	         Ti.API.debug(e.error);
	         //alert('error');
	         dataReturn = "error - check the logs";
	     },
	     timeout : 5000  // in milliseconds
	 });
	 
	 //creates parameters to send to the request
	 var params = {  
    'IDApp':L('id_app'),  
    'Token':L('token')
	}; 
	 // Prepare the connection.
	 client.open("POST", url);
	 // Send the request.
	 client.send(params);
	 
	 return dataReturn;
  
}

function parseJson(value){
	Ti.API.info('JSON PROFILE: ' + value);
	var jsonObject = JSON.parse(value);
		
	Ti.API.info('IDApp: ' + jsonObject.Aplicativo[0].IDApp);
	Ti.API.info('IDUsuario: ' + jsonObject.Aplicativo[0].IDUsuario);
	Ti.API.info('Descricao: ' + jsonObject.Aplicativo[0].Descricao);
	Ti.API.info('---------------------------------------');
		
	webview.setHtml(jsonObject.Aplicativo[0].Descricao);

}

module.exports = CandidateProfileView;