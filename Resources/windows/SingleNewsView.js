/**
 * @author João Henrique Machado Silva
 */

var objData;

function SingleNewsView(title, data, thumbUrl) {
	var self = Ti.UI.createWindow({
		title:data.Titulo,
		backgroundImage:'images/bg.jpg',
		barColor:L('bar_color'),
		barImage: 'images/barraNavegacao.png',
	});
	
	objData = data;
	
	var label = Ti.UI.createLabel({
      font: { fontSize:14 },
      text: data.Data,
      textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
      top: 0,
      left: 10,
      color: L('main_text_color'),
      width: 'auto', 
      height: 'auto'
    });
	
	var thumb =  Titanium.UI.createImageView({
		url:thumbUrl,
		width:280,
		height:150,
		left:20,
		top:20
		});
	
	 var webview = Titanium.UI.createWebView({
	 	top:180,
	 	width:Titanium.Platform.displayCaps.platformWidth,
	 	backgroundColor:'transparent'
	 });
	 webview.html = data.Descricao;
	 
	 self.add(label);
	 self.add(thumb);
	 self.add(webview);
	 
	
	return self;
};

module.exports = SingleNewsView;