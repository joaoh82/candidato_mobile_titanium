/**
 * @author João Henrique Machado Silva
 */

// var objView = require('windows/SingleNewsView');

var tempData = [{title:'Carregando...'}];
var table = Ti.UI.createTableView({data:tempData, backgroundColor:'transparent'});
var tableDataRaw = [];

function TwitterView(title) {
	var self = Ti.UI.createWindow({
		title:L('nome_twitter'),
		backgroundImage:'images/bg.jpg',
		barColor:L('bar_color'),
		barImage: 'images/barraNavegacao.png',
	});
	
	// table.addEventListener('click', function(e) {
    	// Ti.API.log( tableDataRaw[e.index].Titulo );
    	// Ti.API.log( tableDataRaw[e.index].ID );
// 		
		// var obj = tableDataRaw[e.index];
// 		
		// var win = new objView('Noticias', obj);
		// self.containingTab.open(win);
	// });
	
	self.add(table);
	
	getData();
	
	return self;
};

function getData() {
	var url = L('base_url') + L('get_twitter');
	var dataReturn = "";
	 var client = Ti.Network.createHTTPClient({
	     // function called when the response data is available
	     onload : function(e) {
	        // Ti.API.info("Received text: " + this.responseText);
	         //alert('success');
	         
	         parseJson(this.responseText);
	     },
	     // function called when an error occurs, including a timeout
	     onerror : function(e) {
	         Ti.API.debug(e.error);
	         //alert('error');
	         dataReturn = "error - check the logs";
	     },
	     timeout : 5000  // in milliseconds
	 });
	 
	 //creates parameters to send to the request
	 var params = {  
    'IDApp':L('id_app'),  
    'Token':L('token')
	}; 
	 // Prepare the connection.
	 client.open("POST", url);
	 // Send the request.
	 client.send(params);
	 
	 return dataReturn;
  
}

function parseJson(value){
	Ti.API.info('JSON TWITTER: ' + value);
	var jsonObject = JSON.parse(value);
	tableDataRaw = jsonObject.Aplicativo;
	var CustomData = [];
	
	for (var i=0; i < jsonObject.Aplicativo.length; i++) {
		CustomData.push({thumb:jsonObject.Aplicativo[i].Thumb, tweet:jsonObject.Aplicativo[i].Tweet, data:jsonObject.Aplicativo[i].Data,hasChild:false});
		
		  //Ti.API.info('ID: ' + jsonObject.Aplicativo[i].ID);
		  Ti.API.info('Tweet: ' + jsonObject.Aplicativo[i].Tweet);
		  //Ti.API.info('Descricao: ' + jsonObject.Aplicativo[i].Descricao);
		  //Ti.API.info('Thumb: ' + jsonObject.Aplicativo[i].Thumb);
		  //Ti.API.info('Data: ' + jsonObject.Aplicativo[i].Data);
		  Ti.API.info('---------------------------------------');
	};
	Ti.API.info('CustomData Count: ' + CustomData.length);
	
	var tableData=[];
	for (var i=0; i < CustomData.length; i++) {
		
		var row = Titanium.UI.createTableViewRow({backgroundColor:'transparent'});
		 
		var thumb =  Titanium.UI.createImageView({
		url:CustomData[i].thumb,
		width:48,
		height:48,
		left:4,
		top:23
		});
		 
		var titulo = Titanium.UI.createLabel({
		text:CustomData[i].tweet,
		font:{fontSize:14/*,fontWeight:'bold'*/},
		width:'auto',
		color : L('main_text_color'),
		textAlign:'left',
		top:5,
		left:60,
		height:90,
		width:'auto'
		});
		 
		var data =  Titanium.UI.createLabel({
		text:"Data: " + CustomData[i].data,
		font:{fontSize:12,fontWeight:'bold'},
		width:'auto',
		color : L('main_text_color'),
		textAlign:'left',
		top:95,
		bottom:5,
		left:200,
		height:'auto'
		});
		 
		row.add(thumb);
		row.add(titulo);
		row.add(data);
		row.hasChild=CustomData[i].hasChild;
		row.className = 'data_row';
		 
		tableData.push(row);
		Ti.API.info('tableData count: ' + tableData.length);
	};
		
	table.setData(tableData);

}

module.exports = TwitterView;