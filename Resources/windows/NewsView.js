/**
 * @author João Henrique Machado Silva
 */

var objView = require('windows/SingleNewsView');

var tempData = [{title:'Carregando...'}];
var table = Ti.UI.createTableView({data:tempData,
		backgroundImage:'images/bg.jpg'});
var tableDataRaw = [];

function NewsView(title) {
	var self = Ti.UI.createWindow({
		title:title,
		backgroundImage:'images/bg.jpg',
		barColor:L('bar_color'),
		barImage: 'images/barraNavegacao.png'
	});
	
	table.addEventListener('click', function(e) {
    	
		var jsonSelectedObject = JSON.parse(tableDataRaw);
		
		Ti.API.log( jsonSelectedObject.Aplicativo[e.index].Titulo );
    	Ti.API.log( jsonSelectedObject.Aplicativo[e.index].ID );
    	Ti.API.log( jsonSelectedObject.Aplicativo[e.index].Thumb );
		
		var win = new objView('Noticias', jsonSelectedObject.Aplicativo[e.index], jsonSelectedObject.Aplicativo[e.index].Thumb);
		self.containingTab.open(win);
	});
	
	self.add(table);
	
	getData();
	
	return self;
};

function getData() {
	//var url = "http://638ea652dec549f29d80802aae4a0894.cloudapp.net/WebServices/GetNoticias";
	var url = L('base_url') + L('get_news');
	var dataReturn = "";
	 var client = Ti.Network.createHTTPClient({
	     // function called when the response data is available
	     onload : function(e) {
	        // Ti.API.info("Received text: " + this.responseText);
	         //alert('success');
	         
	         parseJson(this.responseText);
	     },
	     // function called when an error occurs, including a timeout
	     onerror : function(e) {
	         Ti.API.debug(e.error);
	         //alert('error');
	         dataReturn = "error - check the logs";
	     },
	     timeout : 5000  // in milliseconds
	 });
	 
	 //creates parameters to send to the request
	 var params = {  
    'IDApp':L('id_app'),  
    'Token':L('token')
	}; 
	 // Prepare the connection.
	 client.open("POST", url);
	 // Send the request.
	 client.send(params);
	 
	 return dataReturn;
  
}

function parseJson(value){
	Ti.API.info('JSON NEWS: ' + value);
	var jsonObject = JSON.parse(value);
	tableDataRaw = value
	var CustomData = [];
	
	Ti.API.info('----------------------------NOTICIAS------------------------');
	for (var i=0; i < jsonObject.Aplicativo.length; i++) {
		CustomData.push({thumb:jsonObject.Aplicativo[i].Thumb, titulo:jsonObject.Aplicativo[i].Titulo, data:jsonObject.Aplicativo[i].Data ,hasChild:true});
		
		  Ti.API.info('ID: ' + jsonObject.Aplicativo[i].ID);
		  Ti.API.info('Titulo: ' + jsonObject.Aplicativo[i].Titulo);
		  Ti.API.info('Descricao: ' + jsonObject.Aplicativo[i].Descricao);
		  Ti.API.info('Thumb: ' + jsonObject.Aplicativo[i].Thumb);
		  Ti.API.info('Data: ' + jsonObject.Aplicativo[i].Data);
		  Ti.API.info('---------------------------------------');
	};
	Ti.API.info('NOTICIAS Count: ' + CustomData.length);
	
	var tableData=[];
	for (var i=0; i < CustomData.length; i++) {
		
		var row = Titanium.UI.createTableViewRow({backgroundColor:'transparent'});
		 
		var thumb =  Titanium.UI.createImageView({
		url:CustomData[i].thumb,
		width:48,
		height:48,
		left:4,
		top:4
		});
		 
		var titulo = Titanium.UI.createLabel({
		text:CustomData[i].titulo,
		font:{fontSize:16,fontWeight:'bold'},
		width:'auto',
		textAlign:'left',
		top:5,
		left:60,
		color : L('main_text_color'),
		height:'auto'
		});
		 
		var data =  Titanium.UI.createLabel({
		text:"Data: " + CustomData[i].data,
		font:{fontSize:12,fontWeight:'bold'},
		width:'auto',
		textAlign:'left',
		bottom:5,
		left:200,
		color : L('main_text_color'),
		height:'auto'
		});
		 
		row.add(thumb);
		row.add(titulo);
		row.add(data);
		row.hasChild=CustomData[i].hasChild;
		row.className = 'data_row';
		 
		tableData.push(row);
		Ti.API.info('tableData count: ' + tableData.length);
	};
		
	table.setData(tableData);

}

module.exports = NewsView;