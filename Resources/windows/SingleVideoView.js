/**
 * @author João Henrique Machado Silva
 */

var objData;

function SingleVideoView(title, data) {
	var self = Ti.UI.createWindow({
		title:data.Nome,
		backgroundImage:'images/bg.jpg',
		barColor:L('bar_color'),
		barImage: 'images/barraNavegacao.png',
	});
	
	objData = data;
	
	var label = Ti.UI.createLabel({
      font: { fontSize:14 },
      text: data.Data,
      textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
      top: 0,
      left: 10,
      color: L('main_text_color'),
      width: 'auto', 
      height: 'auto'
    });
    
    var label2 = Ti.UI.createLabel({
      font: { fontSize:14 },
      text: data.Descricao,
      textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
      color: L('main_text_color'),
      top: 20,
      left: 10,
      width: 'auto', 
      height: 'auto'
    });
	
	 var videoPlayer = Titanium.Media.createVideoPlayer({
        top : 40,
        autoplay : true,
        backgroundColor : 'white',
        height : 'auto',
        width : 'auto',
        backgroundColor: 'transparent',
        mediaControlStyle : Titanium.Media.VIDEO_CONTROL_DEFAULT,
        scalingMode : Titanium.Media.VIDEO_SCALING_ASPECT_FIT
    });
    Ti.API.info(data.IDVideo);
    videoPlayer.url = data.IDVideo;
    self.add(videoPlayer);
    
	 self.add(label);
	 self.add(label2);
	
	return self;
};

module.exports = SingleVideoView;