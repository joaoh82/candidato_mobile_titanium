/**
 * @author João Henrique Machado Silva
 */

var objData;

function SingleEventView(title, data) {
	var self = Ti.UI.createWindow({
		title:data.Titulo,
		backgroundImage:'images/bg.jpg',
		barColor:L('bar_color'),
		barImage: 'images/barraNavegacao.png',
	});
	
	objData = data;
	
	var label = Ti.UI.createLabel({
      font: { fontSize:14 },
      text: data.Data,
      textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
      top: 0,
      color: L('main_text_color'),
      right: 10,
      width: 'auto', 
      height: 'auto'
    });
    
    var local = Ti.UI.createLabel({
      font: { fontSize:14 },
      text: data.Local,
      textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
      top: 20,
      color: L('main_text_color'),
      right: 10,
      width: 'auto', 
      height: 'auto'
    });
	
	 var webview = Titanium.UI.createWebView({
	 	top:45,
	 	backgroundColor:'transparent'
	 });
	 webview.html = data.Descricao;
	 
	 self.add(webview);
	 self.add(label);
	 self.add(local);
	
	return self;
};

module.exports = SingleEventView;